import java.util.Scanner;

public class GameLauncher {
	public static void main (String[] args){
		Scanner scanner = new Scanner(System.in);
		
			System.out.println("Welcome to the games room! To play Hangman, type 1. To play Wordle, type 2.");
				int gameChoice = scanner.nextInt();
				scanner.nextLine();
			
			if (gameChoice == 1){
				System.out.println("Please input a 4 letter word: ");
							String word = scanner.nextLine().toUpperCase();
					WordGuesser.runGame(word);
					}
			else if (gameChoice == 2){
				String word = Wordle.generateWord();
					Wordle.runGame(word);
			}else {
				System.out.println("Please only enter one of the values listed above");
			}
			
	}
}