//2344939
import java.util.Scanner;
import java.util.Random;

/*
Class Wordle will remake the famous NYT "Wordle" game, with a few extra restrictions
All the words will not have any repeating letters, and the game will not keep track of letters you have used
*/

public class Wordle {
	/*
	public static void main(String[] args){ 			//main will assign randomly selected word from generateWord to variable "word", and then call the runGame method using that word
	       String word = generateWord();
			runGame(word);
	}
	*/
	
	public static String generateWord(){ 				//generateWord will select a random word from a pre-defined array and return that word
		Random randGen = new Random(); 
	String [] wordList = new String [] {"WOVEN", "FLORA", "MIRTH", "QUIRK", "ZEBRA", "PLUMB", "DETOX", "FANCY", "JIVED", "FINAL", "QUACK", "GLINT", "HOIST", "JUMPY", "NEXUS", "QUICK", "UNIFY", "BLUES", "WHARF", "ZESTY"};
	int wordInt = randGen.nextInt(wordList.length); 		//get random int that will act as the index for choosing a word in the wordList array
	String word = wordList[wordInt]; 
	return word; 												//returns the randomly selected word
	}
	
	public static boolean letterInWord (String word, char letter) { //letterInWord will check to see if the letter is found somewhere in the word
	for (int i=0; i<5; i++){											//since the word is confined to 5 letters, we can just use 5 in our "for" statement instead of .length()
		if (letter == word.charAt(i)){
			return true;													//returns true if the letter in the guess word matches any other letter in the word (for loop cycles through each letter of the generated word)
			}
		}
			return false;														//returns false if letter does not match any others
	}
	
	public static boolean letterInSlot (String word, char letter, int position){ //letterInSlot will check to see if the letter is found in that slot in that word
		for (int i=0; i<5; i++){												 	//same as line 19
			if (letter == word.charAt(i)){
				if (word.charAt(position) == word.charAt(i)){
					return true;												 		//returns true if the letter is found in the same slot
				}
			}
		}
		return false;																	 //returns false if the letter isn't in the same slot
	}		
	public static String [] guessWord (String answer, String guess){ //guessWord will compare the user's guess with the randomly selected word, and generates a String array based on the correctness of the guessed letters
		String [] colours = new String [answer.length()]; 			 	//creating the new string
		for (int i=0; i<5; i++){
                if (letterInSlot(answer, guess.charAt(i), i)) {
                    colours[i] = "green";							 		//assigns green if letterInSlot returns true (letterInSlot = true means that letterInWord is also true)
                }else if(letterInWord(answer, guess.charAt(i))) {
						colours[i] = "yellow";						 			//assigns yellow if letterInSlot returns false, but letterInWord returns true 
				}else{
					colours[i] = "white";											 //assigns white if both return false
				}	
}
return colours;
}
	public static void presentResults(String guess, String [] colours){ //presentResults will print the letters of the word in colors based off if the letter is in the word, and if it's in the right position 
	
	for (int i = 0; i < guess.length(); i++) {
			if (colours[i] == "green"){ 											
				System.out.print("\u001B[32m" + guess.charAt(i) + "\u001B[0m");
            }
			if (colours[i] == "yellow"){
				System.out.print("\u001B[33m" + guess.charAt(i) + "\u001B[0m"); 			//sets ansi code (colour) depending on outcome of guessWord
			}
			if (colours[i] == "white") {
				System.out.print("\u001B[37m" + guess.charAt(i) + "\u001B[0m");
			}  
		}
		System.out.println(); 
	}


	public static String readGuess(){ 									//readGuess will ask the user to input a 5 letter word, and then store that 5 letter word in a variable called "guess"
		Scanner reader = new Scanner(System.in);
			System.out.println("Please input a 5 letter word: ");
				String guess = reader.nextLine();
				if (guess.length() != 5){									//if the length of the guess is anything other then 5, it inform the user that they must only use 5 letter words as guesses
					System.out.println("5 LETTER WORDS ONLY !");
				}
				String finalGuess = guess.toUpperCase(); 						//converts guess to uppercase, since java is case-sensitive
				return finalGuess;													//returns the users guess in uppercase
	}	
	
	public static void runGame (String word){							//runGame will run the game, giving the user 6 attempts to guess the word
    boolean wordGuessed = false;

    for (int tries = 0; tries < 6 && !wordGuessed; tries++) {			
		String guess = readGuess();											//assign return from readGuess to variable "guess"
		String[] colours = guessWord(word, guess);								//assign return from guessWord to array "colours[]"
		presentResults(guess, colours); 											//call method presentResults to display your guess with colours (green, yellow or white)

    if (word.equals(guess)) {															//if the guess is equal to (the same) as the word, set boolean wordGuessed to true, and print a congratulatory message
		wordGuessed = true;	
		System.out.println("You win!");
        }
    }

    if (!wordGuessed) {																		//if the loop is complete (more then 6 attempts, print out a try again message and end the code
        System.out.println("Try Again :( The word was: " + word);  
    }
}
}




