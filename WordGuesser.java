import java.util.Scanner;

public class WordGuesser {
   /* public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Give a 4 letter word to play Hangman");
        String word = scanner.nextLine().toUpperCase();
        runGame(word);
    } 
	*/

    public static int isLetterInWord(String word, char guess) {
        for (int i = 0; i < word.length(); i++) { 
            if (word.charAt(i) == guess) { 			//check if the letter guessed is equal to any letter in the word
                return i; 								//return index of letter if it is
            }
        }
        return -1;								//return -1 if the letter is not found
    }

    public static char toUpperCase(char guess) {
        return Character.toUpperCase(guess);
    }

    public static void printWord(boolean[] letters, String word) {
        for (int i = 0; i < 4; i++) {								//loops through boolean array, if the letter guessed is not in that spot,
            if (!letters[i]) {										//it prints "_", if it is in that spot, it'll print that letter
                System.out.print(" " + "_" + " ");
            } else {
                System.out.print(" " + word.charAt(i) + " ");
            }
        }
        System.out.println(); 									//to skip to next line
    }

    public static void runGame(String word) {
        Scanner scanner = new Scanner(System.in);
        int misses = 0;
        boolean[] letters = new boolean[]{false, false, false, false};
		boolean condition = true;


        while (condition) {                               //while loop goes through until one of the 
            printWord(letters, word);				//conditions is met, 

            System.out.println("Guess a letter!");
            char guess = toUpperCase(scanner.next().charAt(0));
            int letterIndex = isLetterInWord(word, guess);

            if (letterIndex != -1) {				//if isLetterInWord returns anything other then -1, then the index in letters[]
                letters[letterIndex] = true;		//will be the set to true
            } else {
                misses++;							//or else adds a miss 
            }

            if (misses == 6) {
                System.out.println("You lost! The word was " + word);	//checks if all the guesses have been used
                condition = false;
            }

            boolean allLettersGuessed = true;
            for (boolean letter : letters) {
                if (!letter) {
                    allLettersGuessed = false;	//sets allLettersGuessed to false, when all letters are guessed, it will be turned to true, 
                }
            }

            if (allLettersGuessed) {					//if all are guessed then print "You win" and breaks
                System.out.println("You won! The word was " + word);
                condition = false;
            }
        }
    }
}
